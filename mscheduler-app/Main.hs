module Main where

import Control.Monad
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.Bits
import Data.Maybe
import Data.IORef
import Data.Semigroup
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Control.Monad.STM
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM.TChan
import Control.Concurrent
import qualified STMContainers.Map as STM.Map
import ListT (ListT (..))
import qualified ListT
import Data.Time.Clock.System
import System.Posix.Types (Fd (..))
import GHC.Int (Int64)
import GHC.Event
import Network.GNUnet.Common
import Network.GNUnet.Configuration
import Network.GNUnet.Crypto
import Network.GNUnet.Scheduler
import Network.GNUnet.CADET
import qualified Network.GNUnet.CADET.Types as CADET
import qualified Network.GNUnet.MQ as MQ
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetHelloLib
import Bindings.GNUnet.GnunetTimeLib
import Bindings.GNUnet.GnunetTransportHelloService
import Bindings.GNUnet.GnunetSchedulerLib
import Bindings.GNUnet.GnunetMqLib
import Bindings.GNUnet.GnunetCadetService
import Bindings.GNUnet.GnunetProtocols

{-
C'GNUNET_SCHEDULER_Driver {
    c'GNUNET_SCHEDULER_Driver'cls :: Ptr (),
    c'GNUNET_SCHEDULER_Driver'add :: FunPtr (Ptr () -> Ptr C'GNUNET_SCHEDULER_Task -> Ptr C'GNUNET_SCHEDULER_FdInfo -> CInt),
    c'GNUNET_SCHEDULER_Driver'del :: FunPtr (Ptr () -> Ptr C'GNUNET_SCHEDULER_Task -> CInt),
    c'GNUNET_SCHEDULER_Driver'set_wakeup :: FunPtr (Ptr () -> Bindings.GNUnet.GnunetTimeLib.C'GNUNET_TIME_Absolute -> IO ())
}
-}
type TasksMap = STM.Map.Map (Ptr C'GNUNET_SCHEDULER_Task) FdKey

etts :: C'GNUNET_SCHEDULER_EventType -> Event
etts et = mconcat [ if et .&. c'GNUNET_SCHEDULER_ET_IN /= 0 then evtRead else mempty
                  , if et .&. c'GNUNET_SCHEDULER_ET_OUT /= 0 then evtWrite else mempty ]

timeMs :: IO Int64
timeMs = getSystemTime >>= (\(MkSystemTime s n) -> return $ (s * 1000000) + fromIntegral (n `div` 1000))

validate :: Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> IO CInt
validate _ _ = return C'GNUNET_OK

handle :: Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> B.ByteString -> IO ()
handle _ _ _ = return ()

channelEnded :: Ptr C'GNUNET_CADET_Channel -> IO ()
channelEnded _ = do
    logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Channel ended!\n"
    c'GNUNET_SCHEDULER_shutdown

main :: IO ()
main = do
    logSetup "cadet-test" "ERROR"

    cfg <- load "/home/user/Projects/gnunet/gnunet-hs/gnunet.conf"

    Just evm <- getSystemEventManager
    tim <- getSystemTimerManager

    tasks :: TasksMap <- STM.Map.newIO

    schedH <- newIORef nullPtr -- TODO: tie the knot instead?

    let cb = do
            logNocheck c'GNUNET_ERROR_TYPE_DEBUG "driver: ti_cb\n"
            ret <- c'GNUNET_SCHEDULER_do_work =<< readIORef schedH
            when (ret == C'GNUNET_YES) cb
    timeoutkey :: TVar TimeoutKey <- newTVarIO =<< registerTimeout tim 100000000000 cb -- FIXME: t const
    timeout <- newTVarIO 100000000000

    let add :: Ptr () -> Ptr C'GNUNET_SCHEDULER_Task -> Ptr C'GNUNET_SCHEDULER_FdInfo -> IO CInt
        add _ task fdip = do
            fdi <- peek fdip
            let C'GNUNET_SCHEDULER_FdInfo { c'GNUNET_SCHEDULER_FdInfo'et=et, c'GNUNET_SCHEDULER_FdInfo'sock=sock } = fdi
                cb a b = do
                    logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "driver: ev_cb: " <> show task <> "\n"
                    poke fdip $ fdi { c'GNUNET_SCHEDULER_FdInfo'et=et } -- nim seems to do it inplace
                    c'GNUNET_SCHEDULER_task_ready task fdip
                    ret <- c'GNUNET_SCHEDULER_do_work =<< readIORef schedH
                    when (ret == C'GNUNET_YES) $ cb a b
            let evm_es = etts et
            logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "driver: add: task: " <> show task <> " " <> show fdi <> " " <> show evm_es <> "\n"
            if evm_es == mempty
                then return C'GNUNET_SYSERR
                else do
                    haveIt <- atomically $ STM.Map.lookup task tasks
                    when (haveIt /= Nothing) $ error "OOOOOOOOOOOOOOOOOOOOOOOOO"
                    fdkey <- registerFd evm cb (Fd sock) evm_es OneShot
                    atomically $ STM.Map.insert fdkey task tasks
--                    logNocheck c'GNUNET_ERROR_TYPE_DEBUG =<< (\x -> "driver: add: tasks: " <> x <> "\n") <$> show <$> (atomically . ListT.toList . STM.Map.stream $ tasks)
                    return C'GNUNET_OK

        del :: Ptr () -> Ptr C'GNUNET_SCHEDULER_Task -> IO CInt
        del _ task = do
            logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "driver: del: task: " <> show task <> "\n"
--            logNocheck c'GNUNET_ERROR_TYPE_DEBUG =<< (\x -> "driver: del: tasks: " <> x <> "\n") <$> show <$> (atomically . ListT.toList . STM.Map.stream $ tasks)
          {-
            if app.tasks.take(task, fdi):
              for v in app.tasks.values():
                if v.sock == fdi.sock:
                  return GNUNET_OK
              unregister(AsyncFD(fdi.sock))
          -}
            fdkeys_  <- atomically $ STM.Map.lookup task tasks
            case fdkeys_ of
                -- GNUnet deletes a task, and then cancels it again, and this fails because it's already del'd
                Nothing -> return C'GNUNET_SYSERR
                Just k -> unregisterFd evm k >> atomically (STM.Map.delete task tasks) >> return C'GNUNET_OK

        set_wakeup :: Ptr () -> Bindings.GNUnet.GnunetTimeLib.C'GNUNET_TIME_Absolute -> IO ()
        set_wakeup _ when_t = do
            logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "driver: set_wakeup: " <> show when_t <> "\n"

            k <- readTVarIO timeoutkey

            if when_t == c'GNUNET_TIME_absolute_get_forever_
                -- This rarely leads to aborts at scheduler.c:2272
                then unregisterTimeout tim k >> (atomically $ writeTVar timeout when_t)
                else do
                    curr_t <- fromIntegral <$> timeMs
                    let in_t = if when_t < curr_t then 100 else when_t - curr_t -- Fix overflow
--                    logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "driver: set_wakeup: " <> show (curr_t, in_t) <> "\n"
                    previous_timeout <- readTVarIO timeout

                    let d_recorded_t = abs $ (fromIntegral previous_timeout :: Int64) - (fromIntegral curr_t)
                    let d_proposed_t = (fromIntegral in_t :: Int64)
                    logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "driver: set_wakeup: " <> show (previous_timeout, curr_t, when_t) <> "\n"
                    logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "driver: set_wakeup: " <> show (d_recorded_t, d_proposed_t) <> "\n"
--                    when (d_proposed_t < d_recorded_t) $ do
                    -- Reset the previous timer and set the new value
                    unregisterTimeout tim k

                    k <- registerTimeout tim (fromIntegral in_t) cb
                    atomically $ writeTVar timeoutkey k

                    atomically $ writeTVar timeout when_t


    add' <- mk'GNUNET_SCHEDULER_Driver_add add
    del' <- mk'GNUNET_SCHEDULER_Driver_del del
    set_wakeup' <- mk'GNUNET_SCHEDULER_Driver_set_wakeup set_wakeup

    let driver_ = C'GNUNET_SCHEDULER_Driver nullPtr add' del' set_wakeup'

    with driver_ $ \driver -> do
        writeIORef schedH =<< c'GNUNET_SCHEDULER_driver_init driver

{-
        mh <- c'GNUNET_CADET_connect cfg
        -- FIXME: needs better short-circuiting
        when (nullPtr == mh) $ c'GNUNET_SCHEDULER_shutdown >> undefined
        addShutdown $ c'GNUNET_CADET_disconnect mh

        let target_pid = PeerIdentity . C'GNUNET_PeerIdentity . getCEddsaPublicKey $ ("DHZ51Z5TV3HRMJT0NBYR65A8MBK7D1T3BSTQ4GS57KVNHM1ZF58G" :: EddsaPublicKey)
        let target_port = hash "q"
        let opts = c'GNUNET_CADET_OPTION_DEFAULT .|. c'GNUNET_CADET_OPTION_RELIABLE
        mq_msg_h <- MQ.mkMessageHandler validate handle C'GNUNET_MESSAGE_TYPE_CADET_CLI

        logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Creating to channel\n"
        with mq_msg_h $ \mq_msg_h' -> do
            ch <- createChannel mh target_pid target_port opts Nothing channelEnded mq_msg_h'

            mq <- c'GNUNET_CADET_get_mq ch

            MQ.msg C'GNUNET_MESSAGE_TYPE_CADET_CLI "oooh!\n" >>= c'GNUNET_MQ_send mq
-}

        void . forkIO $ do
            mh <- c'GNUNET_CADET_connect cfg

            shutdownCadet <- mk'GNUNET_SCHEDULER_TaskCallback (const $ c'GNUNET_CADET_disconnect mh)
            void $ c'GNUNET_SCHEDULER_add_delayed 10000 shutdownCadet nullPtr

        replicateM_ 100 $ void . forkIO $ do
            hello_handle <- newIORef nullPtr
            logNocheck c'GNUNET_ERROR_TYPE_DEBUG "task: enter\n"
            writeIORef hello_handle =<< flip (c'GNUNET_TRANSPORT_hello_get cfg c'GNUNET_TRANSPORT_AC_ANY) nullPtr =<< (
                mk'GNUNET_TRANSPORT_HelloUpdateCallback (\_ our_hello_header -> do
                    when (nullPtr == our_hello_header) $ c'GNUNET_SCHEDULER_shutdown >> undefined
                    our_hello <- castPtr <$> c'GNUNET_copy_message our_hello_header
                    c'GNUNET_TRANSPORT_hello_get_cancel =<< readIORef hello_handle
                    alloca $ \our_pid' -> do
                        assert' =<< (== C'GNUNET_OK) <$> c'GNUNET_HELLO_get_id our_hello our_pid'
                        print =<< peek our_pid'
                ))


        atomically $ do
            e <- STM.Map.null tasks
            t <- readTVar timeout
            unless (e && t == c'GNUNET_TIME_absolute_get_forever_) retry

        threadDelay 100000

        logNocheck c'GNUNET_ERROR_TYPE_DEBUG "driver: done!\n"
        readTVarIO timeoutkey >>= unregisterTimeout tim
        c'GNUNET_SCHEDULER_driver_done =<< readIORef schedH
