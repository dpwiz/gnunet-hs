{ mkDerivation, base, bindings-gnunet, bytestring, stdenv, text }:
mkDerivation {
  pname = "gnunet";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [ base bindings-gnunet bytestring text ];
  executableHaskellDepends = [ base bindings-gnunet bytestring ];
  license = stdenv.lib.licenses.agpl3;
}
