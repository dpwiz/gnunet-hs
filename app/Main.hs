module Main where

import Control.Concurrent.MVar
import Control.Monad
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.Bits
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Storable
import Network.GNUnet.Common
import Network.GNUnet.Configuration
import Network.GNUnet.Crypto
import Network.GNUnet.Scheduler
import Network.GNUnet.CADET
import qualified Network.GNUnet.CADET.Types as CADET
import qualified Network.GNUnet.MQ as MQ
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetHelloLib
import Bindings.GNUnet.GnunetSchedulerLib
import Bindings.GNUnet.GnunetTransportHelloService
import Bindings.GNUnet.GnunetMqLib
import Bindings.GNUnet.GnunetCadetService
import Bindings.GNUnet.GnunetProtocols

validate :: Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> IO CInt
-- Replicates gnunet-cadet.c
validate _ _ = return C'GNUNET_OK

handle :: Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> B.ByteString -> IO ()
handle ch msg msg_text = do
    C.putStr msg_text

    mq <- c'GNUNET_CADET_get_mq ch
    -- Echo
    MQ.msg C'GNUNET_MESSAGE_TYPE_CADET_CLI msg_text >>= c'GNUNET_MQ_send mq

    -- Custom reply
    MQ.msg C'GNUNET_MESSAGE_TYPE_CADET_CLI "oooh!\n" >>= c'GNUNET_MQ_send mq

channelIncoming :: Ptr C'GNUNET_CADET_Channel -> PeerIdentity -> IO (Ptr ())
channelIncoming channel initiator = do
    -- TODO: Note we don't close_port, which results in assertion at cadet_api.c:1207
    logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "Incoming connection from " ++ show initiator ++ "\n"

    -- TODO: would be nicer to be IO (Ptr C'GNUNET_CADET_Channel)
    with (CADET.CADETState channel) $ return . castPtr

channelEnded :: Ptr C'GNUNET_CADET_Channel -> IO ()
channelEnded _ = do
    logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Channel ended!\n"
    c'GNUNET_SCHEDULER_shutdown

main :: IO ()
main = do
    logSetup "cadet-test" "DEBUG"

    cfg <- load "gnunet.conf"

    run $ do
        logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Connecting to CADET service\n"
        mh <- c'GNUNET_CADET_connect cfg
        -- FIXME: needs better short-circuiting
        when (nullPtr == mh) $ c'GNUNET_SCHEDULER_shutdown >> undefined
        addShutdown $ do
            c'GNUNET_CADET_disconnect mh

        hello_handle <- newEmptyMVar
        -- our_pid <- newEmptyMVar
        putMVar hello_handle =<< flip (c'GNUNET_TRANSPORT_hello_get cfg c'GNUNET_TRANSPORT_AC_ANY) nullPtr =<< (
            mk'GNUNET_TRANSPORT_HelloUpdateCallback $ \_ our_hello_header -> do
                when (nullPtr == our_hello_header) $ c'GNUNET_SCHEDULER_shutdown >> undefined
                our_hello <- castPtr <$> c'GNUNET_copy_message our_hello_header
                c'GNUNET_TRANSPORT_hello_get_cancel =<< readMVar hello_handle
                alloca $ \our_pid' -> do
                    assert' =<< (== C'GNUNET_OK) <$> c'GNUNET_HELLO_get_id our_hello our_pid'
                    print =<< peek our_pid'
                    -- putMVar our_pid our_pid'
            )

        let target_pid = PeerIdentity . C'GNUNET_PeerIdentity . getCEddsaPublicKey $ ("860X7QR3Y9GA4684DX65QMHTZJDV6ZRK01BN6XWATXEDP3KP3XWG" :: EddsaPublicKey)
        let target_port = hash "q"
        let opts = c'GNUNET_CADET_OPTION_DEFAULT .|. c'GNUNET_CADET_OPTION_RELIABLE
        mq_msg_h <- MQ.mkMessageHandler validate handle C'GNUNET_MESSAGE_TYPE_CADET_CLI

        logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Creating to channel\n"
        with mq_msg_h $ \mq_msg_h' -> do
            ch <- createChannel mh target_pid target_port opts Nothing channelEnded mq_msg_h'

            mq <- c'GNUNET_CADET_get_mq ch

            MQ.msg C'GNUNET_MESSAGE_TYPE_CADET_CLI "oooh!\n" >>= c'GNUNET_MQ_send mq
