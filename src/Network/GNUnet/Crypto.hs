module Network.GNUnet.Crypto
  ( HashCode(..)
  , hash
  , PeerIdentity(..)
  , i2s_full'
  , EddsaPublicKey(..)
  ) where

import Control.Monad
import Data.String (IsString, fromString)
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Data.ByteString.Unsafe (unsafeUseAsCStringLen)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal hiding (void)
import System.IO.Unsafe (unsafeDupablePerformIO)
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCryptoLib
import Bindings.GNUnet.GnunetCryptoTypes

hash :: ByteString -> HashCode
hash x = HashCode . unsafeDupablePerformIO $ alloca $ \r -> do
    unsafeUseAsCStringLen x $ \(s, n) ->
        c'GNUNET_CRYPTO_hash (castPtr @_ @() s) (fromIntegral n) r
    peek r

h2s_full :: C'GNUNET_HashCode -> ByteString
h2s_full h = unsafeDupablePerformIO $ with h $ c'GNUNET_h2s_full >=> B.packCString

newtype HashCode = HashCode { getCHashCode :: C'GNUNET_HashCode } deriving (Eq, Storable)

instance Show HashCode where
    show = T.unpack . TE.decodeLatin1 . h2s_full . getCHashCode

i2s_full :: C'GNUNET_PeerIdentity -> ByteString
i2s_full pid = unsafeDupablePerformIO $ with pid $ c'GNUNET_i2s_full >=> B.packCString

-- FIXME
i2s_full' :: PeerIdentity -> ByteString
i2s_full' = i2s_full . getCPeerIdentity

newtype PeerIdentity = PeerIdentity { getCPeerIdentity :: C'GNUNET_PeerIdentity } deriving (Eq, Storable)

instance Show PeerIdentity where
    show = T.unpack . TE.decodeLatin1 . i2s_full . getCPeerIdentity

newtype EddsaPublicKey = EddsaPublicKey { getCEddsaPublicKey :: C'GNUNET_CRYPTO_EddsaPublicKey } deriving (Eq, Storable)

instance Show EddsaPublicKey where
    show = T.unpack . TE.decodeLatin1 . eddsaPublicKeyToString . getCEddsaPublicKey

instance IsString EddsaPublicKey where
    fromString x = EddsaPublicKey $ case eddsaPublicKeyFromString . TE.encodeUtf8 . T.pack $ x of
        Just a -> a
        _ -> error "Could not parse eddsa public key"

eddsaPublicKeyFromString :: ByteString -> Maybe C'GNUNET_CRYPTO_EddsaPublicKey
eddsaPublicKeyFromString x = unsafeDupablePerformIO $ alloca $ \r -> do
    retcode <- unsafeUseAsCStringLen x $ \(s, n) ->
        c'GNUNET_CRYPTO_eddsa_public_key_from_string s (fromIntegral n) r
    case retcode of
        C'GNUNET_OK -> Just <$> peek r
        _ -> return Nothing

eddsaPublicKeyToString :: C'GNUNET_CRYPTO_EddsaPublicKey -> ByteString
eddsaPublicKeyToString x = unsafeDupablePerformIO $ with x $
    c'GNUNET_CRYPTO_eddsa_public_key_to_string >=> B.packCString
