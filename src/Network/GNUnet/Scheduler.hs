module Network.GNUnet.Scheduler where

import Control.Monad
import Foreign.Ptr
import Bindings.GNUnet.GnunetSchedulerLib

run :: IO () -> IO ()
run action =
    flip c'GNUNET_SCHEDULER_run nullPtr =<< (mk'GNUNET_SCHEDULER_TaskCallback . const $ action)

addShutdown :: IO () -> IO ()
addShutdown action =
    void . flip c'GNUNET_SCHEDULER_add_shutdown nullPtr =<< (mk'GNUNET_SCHEDULER_TaskCallback . const $ action)
