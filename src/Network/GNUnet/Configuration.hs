module Network.GNUnet.Configuration where

import Foreign.Ptr
import Foreign.C.String
import Network.GNUnet.Common (assert')
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetConfigurationLib

load :: FilePath -> IO (Ptr C'GNUNET_CONFIGURATION_Handle)
load path = do
    cfg <- c'GNUNET_CONFIGURATION_create
    assert' =<< (/= C'GNUNET_SYSERR) <$>
        withCString path (c'GNUNET_CONFIGURATION_load cfg)
    return cfg
