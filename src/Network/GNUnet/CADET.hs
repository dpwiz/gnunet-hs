module Network.GNUnet.CADET (openPort, createChannel, CADETState) where

import Data.Maybe (maybe)
import Foreign.Ptr
import Foreign.Storable
import Foreign.C.Types (CInt)
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Bindings.GNUnet.GnunetCryptoTypes (C'GNUNET_PeerIdentity)
import Bindings.GNUnet.GnunetCadetService
import Bindings.GNUnet.GnunetMqLib
import Network.GNUnet.Crypto (HashCode(..), PeerIdentity(..))
import Network.GNUnet.CADET.Types

openPort
  :: Ptr C'GNUNET_CADET_Handle
     -> HashCode
     -> (Ptr C'GNUNET_CADET_Channel
        -> PeerIdentity
        -> IO (Ptr ()))
     -> Maybe (Ptr C'GNUNET_CADET_Channel -> CInt -> IO ())
     -> (Ptr C'GNUNET_CADET_Channel -> IO ())
     -> Ptr C'GNUNET_MQ_MessageHandler
     -> IO (Ptr C'GNUNET_CADET_Port)
openPort handle hash connh wseh dscnh mqmh = do
    connh' <- mk'GNUNET_CADET_ConnectEventHandler $ const $ mkConnH connh
    wseh' <- maybe (return nullFunPtr) mk'GNUNET_CADET_WindowSizeEventHandler $ const <$> wseh
    dscnh' <- mk'GNUNET_CADET_DisconnectEventHandler $ const dscnh
    with (getCHashCode hash) $ \hash' ->
        c'GNUNET_CADET_open_port handle hash' connh' nullPtr wseh' dscnh' mqmh

createChannel
  :: Ptr C'GNUNET_CADET_Handle
     -> PeerIdentity
     -> HashCode
     -> C'GNUNET_CADET_ChannelOption
     -> Maybe (Ptr C'GNUNET_CADET_Channel -> CInt -> IO ())
     -> (Ptr C'GNUNET_CADET_Channel -> IO ())
     -> Ptr C'GNUNET_MQ_MessageHandler
     -> IO (Ptr C'GNUNET_CADET_Channel)
createChannel handle pid hash opts wseh dscnh mqmh = do
    wseh' <- maybe (return nullFunPtr) mk'GNUNET_CADET_WindowSizeEventHandler $ const <$> wseh
    dscnh' <- mk'GNUNET_CADET_DisconnectEventHandler $ const dscnh
    with (getCHashCode hash) $ \hash' ->
        with (getCPeerIdentity pid) $ \pid' ->
            -- FIXME: no synchronisation
            alloca @CADETState $ \cs -> do
                ch <- c'GNUNET_CADET_channel_create handle (castPtr cs) pid' hash' opts wseh' dscnh' mqmh
                poke cs $ CADETState ch
                return ch

mkConnH :: (t -> PeerIdentity -> IO b)
         -> t -> Ptr C'GNUNET_PeerIdentity -> IO b
mkConnH f c i = do
    i' <- PeerIdentity <$> peek i
    f c i'
