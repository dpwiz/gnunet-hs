module Network.GNUnet.CADET.Types (CADETState(..)) where

import Foreign.Ptr
import Foreign.Storable
import Bindings.GNUnet.GnunetCadetService

--TODO: should be extensible
newtype CADETState = CADETState { channel :: Ptr C'GNUNET_CADET_Channel } deriving (Storable)
